package com.zenika.academy.barbajavas.wordle.domain.service.i18n;

import java.io.IOException;
import java.io.InputStream;
import java.text.Normalizer;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Fr extends AbstractI18n {

    public Fr() throws IOException {
        super(ResourceBundle.getBundle("messages", new Locale("fr", "FR")));
    }

    protected void loadDictionnary() throws IOException {
        try (InputStream in = this.getClass().getClassLoader().getResourceAsStream("dico_fr.txt")) {
            Scanner scanner = new Scanner(in);
            while (scanner.hasNext()) {
                String line = scanner.next();
                if (line.length() > 3 && !line.contains("-") && line.length() < 11) {
                    this.dictionary.add(this.removeAccent(line).toUpperCase());
                }

            }
        }
    }

    private String removeAccent(String word) {
        String regex = "[^\\p{ASCII}]";
        return Normalizer.normalize(word, Normalizer.Form.NFD).replaceAll(regex, "");
    }
}