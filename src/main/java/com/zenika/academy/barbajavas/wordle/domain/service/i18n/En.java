package com.zenika.academy.barbajavas.wordle.domain.service.i18n;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Scanner;

public class En extends AbstractI18n {

    public En() throws IOException {
        super(ResourceBundle.getBundle("messages", new Locale("en", "US")));
    }

    @Override
    protected void loadDictionnary() throws IOException {
        try (InputStream in = this.getClass().getClassLoader().getResourceAsStream("dico_en.txt")) {
            Scanner scanner = new Scanner(in);
            while (scanner.hasNext()) {
                String line = scanner.next();
                if (line.length() > 3 && !line.contains("-") && line.length() < 11) {
                    this.dictionary.add(line.toUpperCase());
                }
            }
        }
    }
}
